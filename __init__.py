import csv
import os
import numpy as np
import re
from datetime import datetime, timedelta

# Loading data of covid19 cases into dictionnary
# Needs to download data in ../COVID-19
# git clone https://github.com/CSSEGISandData/COVID-19.git ../COVID-19

f=open(os.path.join('../COVID-19',
                    'csse_covid_19_data',
                    'csse_covid_19_time_series',
                    'time_series_covid19_confirmed_global.csv'))
c=csv.reader(f,delimiter=',')
colnames=next(c)
data={}
for r in c:
    data['%s-%s' % (r[0],r[1])]=np.array([int(v) for v in r[4:]])
f.close()
data_confirmed=data
time_confirmed=[datetime.strptime(v,'%m/%d/%y')+timedelta(days=.5) for v in colnames[4:]]

f=open(os.path.join('../COVID-19',
                    'csse_covid_19_data',
                    'csse_covid_19_time_series',
                    'time_series_covid19_deaths_global.csv'))
c=csv.reader(f,delimiter=',')
colnames=next(c)
data={}
for r in c:
    data['%s-%s' % (r[0],r[1])]=np.array([int(v) for v in r[4:]])
f.close()
data_deaths=data
time_deaths=[datetime.strptime(v,'%m/%d/%y')+timedelta(days=.5) for v in colnames[4:]]

# loading data of population into dictionnary
# data source :  https://population.un.org/

import pandas as pd
df=pd.read_excel('data/WPP2019_POP_F01_1_TOTAL_POPULATION_BOTH_SEXES.xlsx',shee_tname='ESTIMATES',skiprows=16)
data={}
for c,n in zip(df['Country code'],df['2019']):
    data['%03d' % c]=n*1000
data_popul=data

