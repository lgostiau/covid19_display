# covid19_display

Simple Python code to display covid-19 data.

Using 
- Covid19 data from https://github.com/CSSEGISandData/COVID-19 
- Population data from UN https://population.un.org/ (file copied here in data folder)

Installation :

```
git clone https://github.com/CSSEGISandData/COVID-19.git
git clone https://framagit.org/lgostiau/covid19_display.git
cd covid19_display
python3 covid19.py
```

Daily update of Johns Hopkins University Center for Systems Science and Engineering 
 data by `git pull` in COVID-19 folder

Usage :

```
from covid19 import covid_data
c=covid_data('FRA');c.load_data()
c.plot()
```
Result :

![Numbers for a set of countries](img/demo.png)

Recent updates for several countries :

*The number of confirmed cases*
![Numbers for a set of countries](img/Numbers.png)
*The percentage in the population*
![Numbers for a set of countries](img/Percentage.png)
*The percentage in log scale*
![Numbers for a set of countries](img/Percentage_semilog.png)
*Offset in time at the date of one case over 10000*
![Numbers for a set of countries](img/Percentage_semilog_shifted.png)

# See also
This repo is now merging with [Benjamin Pillot project](https://framagit.org/benjaminpillot/covid-19)