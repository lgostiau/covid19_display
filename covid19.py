import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, rrulewrapper,  RRuleLocator, WEEKLY, DAILY
import re
from __init__ import data_confirmed, data_popul, time_confirmed
plt.ion()
plt.close('all')

# git clone https://github.com/CSSEGISandData/COVID-19.git

class covid_data:
    def __init__(self,country):
        import iso3166
        self.country=iso3166.countries_by_alpha3[country]
        self.data={}
        self.param={}
        self.param['population']=data_popul[self.country.numeric]

    def load_data(self):
        from __init__ import data_confirmed,time_confirmed,data_deaths,time_deaths
        self.time_confirmed=np.array(time_confirmed)
        self.time_confirmed_days=np.arange(len(self.time_confirmed))
        self.time_deaths=np.array(time_deaths)
        self.time_confirmed_days=np.arange(len(self.time_deaths))
        data_c={}
        data_d={}
        for l in list(data_confirmed.keys()):
            if self.country.alpha3=='USA':
                name='US'
            elif self.country.alpha3=='KOR':
                name='Korea'
            else:
                name=self.country.name
            if re.match('.*-%s' % name,l):
                data_c[l]=data_confirmed[l]
                data_d[l]=data_deaths[l]
        if len(data_c)==1:
            self.data['confirmed']=list(data_c.values())[0]
            self.data['deaths']=list(data_d.values())[0]
            print('Loading data from row %s' % list(data_c.keys())[0])
        if len(data_c)>1:
            self.data['confirmed']=0
            self.data['deaths']=0
            for k in list(data_c.keys()):
                if (self.country.alpha3=='FRA'):
                    if (k!='-France'):
                        print('Ignoring data from %s' % k)
                        continue
                self.data['confirmed']+=data_c[k]
                self.data['deaths']+=data_d[k]
                print('Loading data from row %s' % k)

    def plot(self,kind='confirmed',percentage=False,timedays=False,semilog=False,time_origin=None):
        y=self.data[kind]
        if percentage:
            y=y/self.param['population']*100
        x=self.time_confirmed
        if timedays:
            x=np.arange(len(y))
        if (time_origin=='1_100000') & percentage:
            found=np.where(y>0.001)[0]
            if len(found>0):
                x-=x[np.argmin(np.abs(y-0.001))]
            else:
                y*=np.nan
        if (time_origin=='1_10000') & percentage:
            found=np.where(y>0.01)[0]
            if len(found>0):
                x-=x[np.argmin(np.abs(y-0.01))]
            else:
                y*=np.nan
        if timedays:
            if semilog:
                p=plt.semilogy(x,y,label=self.country.name)
            else:
                p=plt.plot(x,y,label=self.country.name)
        else:
            p=plt.plot_date(x,y,'-',label=self.country.name)
            plt.gca().xaxis.set_major_formatter(DateFormatter('%d/%m'))
            if len(x)<50: interval=10
            elif len(x)<100: interval=15
            else: interval=20
            rule = rrulewrapper(DAILY,interval=interval)
            loc = RRuleLocator(rule)
            plt.gca().xaxis.set_major_locator(loc)
        if timedays:
            if time_origin=='1_100000':
                plt.xlim((-20,30))
                plt.ylim((1e-5,1))
                plt.xlabel('Time (days), origin at 0.001% contamination')
            else:
                plt.xlabel('Time (days from %s)' % self.time_confirmed[0].strftime('%Y/%m/%d'))
        else:
            plt.xlabel('Date (2020)')
        if percentage:
            plt.ylabel('confirmed, % of total pop.')
        else:
            plt.ylabel('# of confirmed cases')
        plt.legend()
        plt.grid('on')
        return p

                
if __name__=='__main__':
    
    from importlib import reload
    import covid19
    reload(covid19)

    cdata=[]
    for country in ['FRA','ITA','ESP','USA','PRT','AUT','IRL','POL','JPN','DEU','KOR']:
        c=covid19.covid_data(country)
        c.load_data()
        cdata.append(c)
    plt.figure(figsize=(16,10))
    for c in cdata:
        p=c.plot(kind='confirmed',percentage=False,timedays=False,semilog=False)[0]
        if c.country.alpha3=='FRA': p.set_linewidth(3)
    plt.title('Data at %s' % c.time_confirmed[-1].strftime('%Y/%m/%d')) 
    plt.savefig('Numbers')
    plt.figure(figsize=(16,10))
    for c in cdata:
        p=c.plot(percentage=True,timedays=False,semilog=False)[0]
        if c.country.alpha3=='FRA': p.set_linewidth(3)
    plt.title('Data at %s' % c.time_confirmed[-1].strftime('%Y/%m/%d')) 
    plt.savefig('Percentage')
    plt.figure(figsize=(16,10))
    for c in cdata:
        p=c.plot(percentage=True,timedays=True,semilog=True)[0]
        if c.country.alpha3=='FRA': p.set_linewidth(3)
    plt.title('Data at %s' % c.time_confirmed[-1].strftime('%Y/%m/%d')) 
    plt.savefig('Percentage_semilog')

    plt.figure(figsize=(16,10))
    for c in cdata:
        p=c.plot(percentage=True,timedays=True,semilog=True,time_origin='1_100000')[0]
        if c.country.alpha3=='FRA': p.set_linewidth(3)
    plt.title('Data at %s' % c.time_confirmed[-1].strftime('%Y/%m/%d')) 
    plt.xlim((-20,80))
    plt.savefig('Percentage_semilog_shifted')


    
    c=covid_data('FRA');
    c.load_data()
    plt.figure()
    c.plot()
    plt.savefig('demo')


